/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.options module, one of many modules that belongs to smoslt

 smoslt.options is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.options is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.options in the file smoslt.options/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.options;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.impl.score.director.ScoreDirector;
import org.optaplanner.core.impl.score.director.ScoreDirectorFactory;

public class Solve {
	public final String SOLVER_CONFIG_XML = "smoslt/options/optionsSolverConfig.xml";
	public static ScoreDirector scoreDirector;

	public OptionsSolution go(OptionsSolution options) {
		Solver solver = SolverFactory.createFromXmlResource(SOLVER_CONFIG_XML)
				.buildSolver();
		ScoreDirectorFactory scoreDirectorFactory = solver
				.getScoreDirectorFactory();
		scoreDirector = scoreDirectorFactory.buildScoreDirector();
		scoreDirector.setWorkingSolution(options);
		solver.solve(options);
		return (OptionsSolution) solver.getBestSolution();
	}

}
