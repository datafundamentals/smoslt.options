/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.options module, one of many modules that belongs to smoslt

 smoslt.options is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.options is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.options in the file smoslt.options/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.options.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.optaplanner.core.impl.score.director.ScoreDirector;

import smoslt.options.OptionEntity;
import smoslt.options.OptionsSolution;
import smoslt.options.Solve;

public class HelloApp {
//	public static final String SOLVER_CONFIG_XML = "org/btrg/optaplannerhellotest/solver/helloCounterSolverConfig.xml";
//	public static ScoreDirector scoreDirector;

//	public static void main(String[] args) {
//		List<Option> optionList = new ArrayList<Option>();
//		for (int i = 0; i < 100; i++) {
//			optionList.add(new Option(UUID.randomUUID().toString()));
//		}
//		Options options = new Options();
//		options.setOptionList(optionList);
//		Options bestOptions = (Options) new Solve().go(options);
//		System.out.println("\n\nBEST SOLUTION HAS EXACTLY "
//				+ bestOptions.getOptionList().get(0).getOnOff() + "\t"
//				+ bestOptions.getOptionList().get(0).getId());
//		for (Option option : bestOptions.getOptionList()) {
//			System.out.println(option.getId().hashCode() + "\t"
//					+ option.getOnOff());
//		}
//	}

}
