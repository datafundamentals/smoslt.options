/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.options module, one of many modules that belongs to smoslt

 smoslt.options is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.options is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.options in the file smoslt.options/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.options;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.bendable.BendableScore;

import smoslt.optionsapi.Option;
import smoslt.optionsapi.Options;
import smoslt.optionsapi.ScoreKeeper;

@PlanningSolution
public class OptionsSolution implements Solution<BendableScore>, Serializable {
	private static final long serialVersionUID = -6806897853254855720L;
	private List<OptionEntity> optionEntityList;
	private List<Boolean> optionValues;
	private BendableScore score;
	private ScoreKeeper scoreKeeper;

	public OptionsSolution() {
		populateOnOffBooleans();
	}
	
	public void setScoreKeeper(ScoreKeeper scoreKeeper){
		this.scoreKeeper = scoreKeeper;
	}

	@PlanningEntityCollectionProperty
	public List<OptionEntity> getOptionEntityList() {
		return optionEntityList;
	}

	@ValueRangeProvider(id = "onOffRange")
	public List<Boolean> getOnOffList() {
		return optionValues;
	}

	public void setOptionEntityList(List<OptionEntity> optionList) {
		this.optionEntityList = optionList;
	}

	public void setOptionNameList(List<String> optionNames) {
		optionEntityList = new ArrayList<OptionEntity>();
		for (String name : optionNames) {
			OptionEntity option = new OptionEntity(name);
			optionEntityList.add(option);
		}
	}

	public Collection<? extends Object> getProblemFacts() {
		List<Object> facts = new ArrayList<Object>();
		facts.add(this); //for ScoreKeeper to consume
		facts.add(scoreKeeper);
		return facts;
	}

	private void populateOnOffBooleans() {
		if (null == optionValues) {
			optionValues = new ArrayList<Boolean>();
			optionValues.add(true);
			optionValues.add(false);
		}
	}

	@Override
	public BendableScore getScore() {
		BendableScore bendableScore = (BendableScore) Solve.scoreDirector
				.calculateScore();
		System.out.println("BENDABLE SCORE " + bendableScore.getHardScore(0));
		System.out.println("BENDABLE SCORE " + bendableScore.getSoftScore(0));
		System.out.println("BENDABLE SCORE " + bendableScore.getSoftScore(1));
		System.out.println("BENDABLE SCORE " + bendableScore.getSoftScore(2));
		System.out.println("BENDABLE SCORE " + bendableScore.getSoftScore(3));
		System.out.println("BENDABLE SCORE " + bendableScore.getSoftScore(4));
		return bendableScore;
	}

	@Override
	public void setScore(BendableScore score) {
		if (("" + score).startsWith("0")) {
			// System.out.println(" SET SCORE " + score);
		} else {
			// System.out.println(" SET SCORE " + score +"\t"+
			// optionList.size());
		}
		System.out.println(" SET SCORE " + score);
		this.score = score;
	}

	public Options getOptions() {
		Options options = new Options();
		for (OptionEntity optionEntity : optionEntityList) {
			options.addOption(new Option(optionEntity.getId(), optionEntity
					.getOnOff()));
		}
		return options;
	}
}
